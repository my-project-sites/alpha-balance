=== Easy HTML Sitemap ===
Contributors:  a.ankit , sayful
Donate link: https://websitebuilderguide.com
Tags: sitemap, html sitemap, display sitemap, list, page list
Requires at least: 3.0
Tested up to: 5.3.2
Stable tag: 1.4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easy HTML Sitemap - Display an HTML Sitemap for your wordpress pages using a shortcode. The sitemap is updated in realtime.

== Description ==

Easy HTML Sitemap plugin makes it super easy for you add a sitemap page on your website. HTML Sitemaps are great for website visitors.


= Easy HTML Sitemap Plugin Features: =

   1. Output a list of Pages with a simple shortcode. New pages will automatically appear in the sitemap as soon as they are added
   2. Include Multiple Post types in the sitemap
   3. Customize the HTML Wrappers to style post types in different ways
   4. Display Post Type name as Labels
   5. Exclude Specific posts by id
   6. Open links in new tab
   7. Order the sequenece in which the posts appear. 
   8. Display Category based Sitemap. Ideal for displaying sitemap for blog posts (coming soon)
  

For more support visit [Easy HTML Sitemap](https://websitebuilderguide.com/wp-plugins/easy-html-sitemap-plugin-for-wordpress/) plugin page. We will be glad to help you.

= Please Vote and Enjoy =
Your votes really make a difference! Thanks.

= How to add FAQs on your site: =

      1. Add this [easy-html-sitemap] shortcode to the page/post where you want to show the sitemap
     
= Configuration: =
   
      1. Got to Settings->Easy HTML Sitemap and find the options there 
   
== Installation ==
The installation of this plugin is very simple. It takes only few minutes. There are two ways to install it:

   a.Install using FTP
      
      1. Download 'Easy HTML Sitemap' plugin.
      2. Unzip and upload 'Easy HTML Sitemap' folder to the /wp-content/plugins/ directory of your WordPress setup.
      3. Activate the plugin from the 'Plugins' menu option in WordPress admin.
      
   b.Install using Upload method in WordPress admin panel

      1. Download 'Easy HTML Sitemap' plugin.
      2. Click on 'Plugins' menu option in WordPress admin.
      3. Click on 'Add New' option in plugins.
      4. Upload the 'Easy HTML Sitemap' plugin and 'Activate' it.

Updating the Sitemap:

      1. The sitemap is updated in realtime.



== Frequently Asked Questions ==

= Can I include multiple post types in the sitemap? =

Yes. You may include any type of post usng show_post_types attribute. The default value is Page. 

= Can I customize the SiteMap Design? =

Yes, you may target the sitemap using css. 

= Can I exclude specific posts? =

Yes, you can exclude specific posts by specifying their id.  



== Changelog ==


= 1.4.9 =
* MInor changes in help section. Shared features under consideration 


= 1.4.8 =
* New attribue. limit_item. 

= 1.4.7 =
* Two New attribues. order_by and order. 


= 1.4.6 =
* Fixed a discrepancy in the help instruction. It had resulted in users entering wroong values in the show_post_type attribute

= 1.4.5 =
* New attribute open_new_tab. 

= 1.4.4 =
* Added Settings link under the Plugin Menu

= 1.4.2 =
* Added a New Tab for Feature Request

= 1.4.1 =
* Added a Feature Request Text

= 1.4 =
* Minor Changes in Display

= 1.3.5 =
* Layout bug fixed

= 1.3.4 =
* Small Fix

= 1.3.3 =
* Small Bug Fix

= 1.3.1 =
* Small Bug Fix
* Minor change in layout

= 1.3.1 =
* Small Bug Fix

= 1.3 =
* New Exclude attribute
* MInor changes in Layout

= 1.2 =
* MInor changes in Layout


= 1.1 =
* INtroduced attribute support
* Possible to display multiple post types in the sitemap.
* The post type label is wrapped in H2 Tag by default


= 1.0 =
* First Version

