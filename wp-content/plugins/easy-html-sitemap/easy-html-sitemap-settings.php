

<div class="wrap settings-wrap" id="page-settings">
    <h2><?php _e('Settings','wb-ehs') ?></h2>
    <div id="option-tree-header-wrap">
        <ul id="option-tree-header">
            <li id=""><a href="" target="_blank"></a>
            </li>
            <li id="option-tree-version"><span><?php _e('Easy HTML SiteMap','wb-ehs') ?> </span>
            </li>
        </ul>
    </div>
    <div id="option-tree-settings-api">
    <div id="option-tree-sub-header"></div>
        <div class = "ui-tabs ui-widget ui-widget-content ui-corner-all">
           
          <!-- Tabs Begin-->
            <ul >
               
            
          
            
                <li id="tab_shortcode_atts" ><a href="#shortcode_atts"><?php _e('Shortcode Attributes','wb-ehs') ?></a>
                </li>
              <!--  <li id="tab_layouts" ><a href="#section_roadmap">RoadMap</a>
                </li> -->
                
                <li id="tab_faq" ><a href="#section_faq"><?php _e('FAQ','wb-ehs') ?></a>
                </li>
                <li id="tab_support" ><a href="#section_support"><?php _e('Support','wb-ehs') ?></a>
                </li>
              
              <li id="tab_support" ><a href="#section_feature_request"><?php _e('Feature Request','wb-ehs') ?></a>
                </li>
               
            </ul>
            <!-- Tabs End-->
            
            
    <div id="poststuff" class="metabox-holder">
        <div id="post-body">
			<div id="post-body-content">
          
	

	
	<div id="shortcode_atts" class = "postbox">
        <div class="inside">
            <div class="format-settings">
                <div class="format-setting-wrap">
    <form method = "post" action="#shortcode_atts">
		<div class="format-setting-label">
			<h3 class="label"><?php _e('Shortcode Attributes','wb-ehs') ?></h3>
		</div>
    </form>
	<p><strong><?php _e('How to use:','wb-ehs') ?></strong> [easy-html-sitemap]</p>
	<p class="description"><?php echo sprintf(__('<strong>Note: We have added support for shortcode attributes since version 1.1</strong> ','wb-ehs')); ?></p>
  <p><strong><?php _e('Example usage:','wb-ehs') ?></strong> [easy-html-sitemap show_post_types = 'post,page']</p>
	

	<table id="menu-locations-table" class="widefat fixed">
		<thead>
		<tr>
		<th class="wff-att-name" scope="col"><?php _e('Shortcode Attribute','wb-ehs') ?></th>
		<th class="wff-att-name" scope="col"><?php _e('Value (for Example)','wb-ehs') ?></th>
		<th class="wff-att-uses" scope="col"><?php _e('Uses','wb-ehs') ?></th>
		</tr>
		</thead>
		<tbody>
		<tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'show_post_types'; ?></td>
			<td class="wff-att-name"><?php echo 'page,post'; ?></td>
			<td class="wff-att-uses"><?php _e('These are the post types which you want to include in the sitemap. Default value is page. ','wb-ehs') ?></td>
		</tr>
		<tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'is_show_title'; ?></td>
			<td class="wff-att-name"><?php echo 'No'; ?></td>
			<td class="wff-att-uses"><?php _e('If you want to display the Post Type Label. Default is yes. The label is wrapped in H2 tag by default','wb-ehs') ?></td>
		</tr>
			<tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'exclude'; ?></td>
			<td class="wff-att-name"><?php echo '25,67'; ?></td>
			<td class="wff-att-uses"><?php _e('If you want to exclude specific posts from the sitemap then use the Exclude attribute. By default all posts are displayed.','wb-ehs') ?></td>
		</tr>
	
			<tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'open_new_tab'; ?></td>
			<td class="wff-att-name"><?php echo 'yes'; ?></td>
			<td class="wff-att-uses"><?php _e('If you want to open the sitemap links in new tab, then you can make use of this attribute. The default value is set as NO.','wb-ehs') ?></td>
		</tr>
	
      
      <tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'order_by'; ?></td>
			<td class="wff-att-name"><?php echo 'post_title'; ?></td>
			<td class="wff-att-uses"><?php _e('order_by can take comma-separated list of column names to sort the pages/posts/custom posts by. <br> Accepts post_author,post_date, post_title, post_name, post_modified, post_modified_gmt, menu_order, post_parent, ID, rand, or comment_count. <br> Default is post_title.','wb-ehs') ?></td>
		</tr>
	
       <tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'order'; ?></td>
			<td class="wff-att-name"><?php echo 'asc'; ?></td>
			<td class="wff-att-uses"><?php _e('order can take desc or asc. Default asc.','wb-ehs') ?></td>
		</tr>
		
        <tr id="menu-locations-row">
			<td class="wff-att-name"><?php echo 'limit_item'; ?></td>
			<td class="wff-att-name"><?php echo '4'; ?></td>
			<td class="wff-att-uses"><?php _e('You can limit the number of items to display using this attribute. <br>This ONLY works for non-hierarchial posts. So it will not work for pages but it will work for posts. <br> Similarly it will work for a custom post type which is non-hierarchial in nature.','wb-ehs') ?></td>
		</tr>
		
		</tbody>
	</table>	
			   </div>
            </div>
        </div>
	</div>
    <!-- Raodmap Section - Ignore
	<div id="section_roadmap" class = "postbox">
		<div class="inside">
            <div id="setting_modify_layouts_text" class="format-settings">
                <div class="format-setting-wrap">
                    <div class="format-setting-label">
                    <h3 class="label">Future RoadMap </h3>
                    </div>
                </div>
            </div>
                                
        <p>We have big plans for the FaceBook Feed plugin. Here is a tentative RoadMap  </p>
        <p><span class="description"></span> In future versions we will provide support for: </span></p>
        <p><span class="description">1. Design Customizations.</span></p>
        <p><span class="description">2. Text / Font Size customization using Admin Panel</span></p>
        <p><span class="description">3. Support for Custom CSS</span></p>
        <p><span class="description">4. Support for Date Format Customization</span></p>
        <p><span class="description">5. Multiple COlor Schemes.</span></p>
        <p><span class="description">6. HasTag Linking.</span></p>
        <p><span class="description">7. Support for Multiple Facebook Feeds.. And Lots More</span></p>
                        
        </div>
    </div>
    -->
                        
    <div id="section_faq" class = "postbox">
        <div class="inside">
            <div class="format-settings">
                <div class="format-setting-wrap">
                    <div class="format-setting-label">
                    <h3 class="label"><?php _e('FAQ','wb-ehs') ?> </h3>
                    </div>
                </div>
            </div>
                                
        <p><span class="description"><?php _e('1. Use the Shortcode <strong> [easy-html-sitemap]</strong> to display an html sitemap on any page or post','wb-ehs') ?></span></p>
        <p><span class="description"><?php _e('','wb-ehs') ?></span></p>
                      
				</div>
	</div>
	
	
	  <div id="section_support" class = "postbox">
        <div class="inside">
            <div class="format-settings">
                <div class="format-setting-wrap">
                    <div class="format-setting-label">
                    <h3 class="label"><?php _e('Support','wb-ehs') ?> </h3>
                    </div>
                </div>
            </div>
                                
       <p><span class="description"><?php echo sprintf(__("1. For any queries contact us via the <a href='https://wordpress.org/support/plugin/easy-html-sitemap/' target='_blank'>support forums.</a>","wb-ehs")); ?></span></p>
        
          
                                     
       <p><span class="description"><?php echo sprintf(__("2. <b>Request a feature</b> by filling out <a href = 'https://websitebuilderguide.com/feature-request-easy-html-sitemap-plugin/' target = '_blank'>this form</a>. We will try our best to implement your suggestions in future releases.","wb-ehs")); ?></span></p>
       
                      
				</div>
	</div>
	
	
        
        
        <!-- Feature Request Tab -->
        
        
        	  <div id="section_feature_request" class = "postbox">
        <div class="inside">
            <div class="format-settings">
                <div class="format-setting-wrap">
                    <div class="format-setting-label">
                    <h3 class="label"><?php _e('Feature request','wb-ehs') ?> </h3>
                    </div>
                </div>
            </div>
                                
              
                                     
       <p><span class="description"><?php echo sprintf(__("1. <b>Request a feature</b> by filling out <a href = 'https://websitebuilderguide.com/feature-request-easy-html-sitemap-plugin/' target = '_blank'>this form</a>. We will try our best to implement your suggestions in future releases.","wb-ehs")); ?></span></p>
                                       
       <p><span class="description"><?php echo sprintf(__(" <b>We would love to hear from you.</b>","wb-ehs")); ?></span></p>
          
            <p><span class="description"><?php echo sprintf(__(" <b>Features Under Consideration.</b>","wb-ehs")); ?></span></p>
       <p><span class="description"><?php echo sprintf(__(" 1. Create Sitemap from Nav Menu","wb-ehs")); ?></span></p>
       <p><span class="description"><?php echo sprintf(__(" 2. Horizontal Layout.","wb-ehs")); ?></span></p>
       <p><span class="description"><?php echo sprintf(__(" 3. Tabbed Layout","wb-ehs")); ?></span></p>
     <p><span class="description"><?php echo sprintf(__(" 4. More options to Sort and filter the output","wb-ehs")); ?></span></p>
     
                      
				</div>
	</div>
	
        
        <!-- End of Feature Request tab-->

	
	
	
	
        </div>
    </div>
    </div>
        <div class="clear"></div>
        </div>
    </div>
</div>