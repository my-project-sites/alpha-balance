<?php
/*
Plugin Name:  Easy HTML Sitemap
Plugin URI:   https://websitebuilderguide.com/wp-plugins/easy-html-sitemap-plugin-for-wordpress/
Description:  Easily add an HTML Sitemap on your website. No coding required. 
Version:      1.4.9
Author:       a.ankit
Author URI:   https://websitebuilderguide.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wb-ehs
Domain Path:  /lang
*/


function wbehs_activate() {

	// Return if the option value already exists
	if ( get_option( 'wbehs_installed_status' ) ) {
		return;
	}

	// Create if the value does not exist
	update_option( 'wbehs_installed_status', '2' );
}

register_activation_hook( __FILE__, 'wbehs_activate' );


function wbehs_register_options_page() {
	add_options_page( 'Easy HTML Sitemap Settings', 'Easy HTML Sitemap',
		'manage_options', 'wbehs_settings', 'wbehs_create_ehs_settings_page' );
}

add_action( 'admin_menu', 'wbehs_register_options_page' );


function wbehs_create_ehs_settings_page() {
	require_once( 'easy-html-sitemap-settings.php' );
}


/** Added the Settings link in the Plugin List Page**/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wbehs_add_action_links' );

function wbehs_add_action_links( $links ) {
	$mylinks = array(
		'<a href="' . admin_url( 'options-general.php?page=wbehs_settings' ) . '">Settings</a>',
	);

	return array_merge( $links, $mylinks );
}


function wbehs_load_custom_wp_admin_style( $hook ) {
	// if ($hook != 'toplevel_page_wbehs_settings'){return;} // we dont want to load our css on other pages
	wp_register_style( 'wbehs_custom_wp_admin_css', plugins_url( 'css/wff-admin.css', __FILE__ ) );
	wp_enqueue_style( 'wbehs_custom_wp_admin_css' );
	//  wp_enqueue_style( 'wp-color-picker' ); // here we add the color picker style for use in our plugin

}

add_action( 'admin_enqueue_scripts', 'wbehs_load_custom_wp_admin_style' );


function wbehs_load_custom_wp_admin_scripts( $hook ) {

// if ($hook != 'toplevel_page_wbehs_settings'){return;} // we dont want to load our js on other pages

	wp_register_script( 'wbehs_custom_wp_admin_js', plugin_dir_url( __FILE__ ) . 'js/my-custom.js',
		array( 'jquery', 'jquery-ui-core', 'jquery-ui-tabs', 'wp-color-picker' ), false, '1.0.0' );
	wp_enqueue_script( 'wbehs_custom_wp_admin_js' );

}

add_action( 'admin_enqueue_scripts', 'wbehs_load_custom_wp_admin_scripts' );

/**
 * Display Link
 *
 *
 */

function wbehs_display_link() {
	// 3 will go to homepage
	if ( get_option( 'wbehs_installed_status' ) == 2 ) { ?>
        <div style="text-align:right; font-size:11px;">Easy HTML Sitemap Plugin by <a title="Best Website Builders"
                                                                                      href="https://websitebuilderguide.com/"
                                                                                      target="_blank">WebsiteBuilderGuide</a>
        </div>
	<?php }
}

/**
 * Filters the HTML attributes applied to a page menu item's anchor element.
 *
 * @param array $atts {
 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
 *
 * @type string $href The href attribute.
 * @type string $aria_current The aria-current attribute.
 * }
 * @return array
 */
function wbehs_page_menu_link_attributes( $atts, $page, $depth, $args ) {
	if ( isset( $args['open_new_tab'] ) && in_array( $args['open_new_tab'], [ 'yes', 'true', 'on', 1, true ], true ) ) {
		$atts['target'] = '_blank';
	}

	return $atts;
}

add_filter( 'page_menu_link_attributes', 'wbehs_page_menu_link_attributes', 10, 4 );


/**
 * Display hierarchical post
 *
 * @param array $atts
 * @param \WP_Post_Type $obj
 */
function wbehs_hierarchical_post( $atts, $obj ) {
	$title          = ( 'yes' == $atts['is_show_title'] ) ? $obj->label : '';
	$sort_column    = isset( $atts['order_by'] ) ? $atts['order_by'] : 'post_title';
	$sort_order     = 'desc' == $atts['order'] ? 'DESC' : 'ASC';
	$post_type_list = wp_list_pages( array(
		'title_li'     => '',
		'echo'         => 0,
		'exclude'      => $atts['exclude'],
		'post_type'    => $obj->name,
		'open_new_tab' => ( 'yes' == $atts['open_new_tab'] ) ? 'yes' : 'no',
		'sort_order'   => $sort_order,
		'sort_column'  => $sort_column,
	) );
	?>
    <div class="easy-html-sitemap__item post-type-<?php echo esc_attr( $obj->name ); ?>">
		<?php
		if ( ! empty( $title ) && ! empty( $post_type_list ) ) {
			$title_tag = $atts['title_tag'];
			echo '<' . esc_attr( $title_tag ) . ' class="easy-html-sitemap__item-title">' . esc_html( $title ) . '</' . esc_attr( $title_tag ) . '>';
		}
		?>

		<?php echo '<ul class="easy-html-sitemap__list">' . $post_type_list . '</ul>'; ?>
    </div>
	<?php
}

/**
 * Display non hierarchical post
 *
 * @param array $atts
 * @param \WP_Post_Type $obj
 */
function wbehs_non_hierarchical_post( $atts, $obj ) {
	$orderby    = isset( $atts['order_by'] ) ? $atts['order_by'] : 'post_title';
	$order      = 'desc' == $atts['order'] ? 'DESC' : 'ASC';
	$limit_item = is_numeric( $atts['limit_item'] ) && intval( $atts['limit_item'] ) > 1 ? $atts['limit_item'] : - 1;

	/** @var \WP_Post[] $_posts */
	$_posts = get_posts( array(
		'post_type'      => $obj->name,
		'posts_per_page' => $limit_item,
		'post_status'    => 'publish',
		'exclude'        => $atts['exclude'],
		'orderby'        => $orderby,
		'order'          => $order,
	) );

	$title  = ( 'yes' == $atts['is_show_title'] ) ? $obj->label : '';
	$target = ( 'yes' == $atts['open_new_tab'] ) ? 'target="_blank"' : '';
	?>
    <div class="easy-html-sitemap__item post-type-<?php echo esc_attr( $obj->name ); ?>">
		<?php
		if ( ! empty( $title ) && count( $_posts ) ) {
			$title_tag = $atts['title_tag'];
			echo '<' . esc_attr( $title_tag ) . ' class="easy-html-sitemap__item-title">' . esc_html( $title ) . '</' . esc_attr( $title_tag ) . '>';
		}
		?>
        <ul class="easy-html-sitemap__list">
			<?php foreach ( $_posts as $post ) {
				$title = ! empty( $post->post_title ) ? $post->post_title : '#' . $post->ID . ' (no title)';
				?>
                <li class="page_item page-item-<?php echo esc_attr( $post->ID ); ?>">
                    <a href="<?php echo esc_url( get_permalink( $post ) ); ?>" <?php echo $target; ?>><?php echo esc_html( $title ); ?></a>
                </li>
			<?php } ?>
        </ul>
    </div>
	<?php
}

/**
 * @param array $atts Shortcode attributes
 *
 * @return string
 */
function wbehs_display_sitemap( $atts ) {
	$atts = shortcode_atts( array(
		'show_post_types' => 'page',
		'is_show_title'   => 'yes',
		'title_tag'       => 'h2',
		'exclude'         => '',
		'open_new_tab'    => 'no',
		'order_by'        => 'post_title',
		'order'           => 'asc',
		'limit_item'      => '-1',
	), $atts, 'easy-html-sitemap' );

	// If title tag is not valid heading tag, then set h2 as default
	if ( ! in_array( $atts['title_tag'], array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ) ) ) {
		$atts['title_tag'] = 'h2';
	}


	$post_types = trim( $atts['show_post_types'], ',' );
	$post_types = explode( ',', $post_types );
	ob_start();
	?>
    <div class="easy-html-sitemap">

		<?php
		foreach ( $post_types as $post_type ) {
			if ( ! post_type_exists( $post_type ) ) {
				new WP_Error( "post_type_not_exists", "{$post_type} post type does not exists." );
				continue;
			}

			/** @var \WP_Post_Type $obj */
			$obj = get_post_type_object( $post_type );

			if ( $obj->hierarchical ) {
				wbehs_hierarchical_post( $atts, $obj );
			} else {
				wbehs_non_hierarchical_post( $atts, $obj );
			}
		}

		wbehs_display_link();

		?>

    </div> <!-- this is closing div of sitemap wrapper-->
	<?php

	return ob_get_clean();
}

add_shortcode( 'easy-html-sitemap', 'wbehs_display_sitemap' );