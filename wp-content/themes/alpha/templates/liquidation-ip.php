<?php 
    # Template Name: Ликвидация ИП
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <h1 class="title title_first">Ликвидация ИП</h1>

                <p>
                    Любой гражданин, имеющий статус индивидуального предпринимателя, в любой момент имеет право закончить свою деятельность. Но нельзя просто прекратить сдавать отчётность, необходимо выполнить процедуру ликвидации ИП. Закрытие ИП носит уведомительный характер. Это означает, что ликвидация ИП будет осуществлена на основании заявления от физического лица.
                </p>
            </div>

            <div class="col-md-6">
                <h3 class="title title_third">Документы, которые необходимо предоставить:</h3>

                <ul class="markedList">
                    <li class="markedList__item">Паспортные данные индивидуального предпринимателя.</li>
                    <li class="markedList__item">паспорт гражданина РФ.</li>
                    <li class="markedList__item">ИНН.</li>
                    <li class="markedList__item">ОГРНИП (Свидетельство ИП).</li>
                    <li class="markedList__item">Выписку из ПФР, свидетельствующую об отсутствии задолженности по платежам.</li>
                </ul> 

                <h3 class="title title_third">Ликвидация ИП: цена:</h3>

                <ul class="markedList">
                    <li class="markedList__item">Государственную пошлину – 200 ₽.</li>
                    <li class="markedList__item">Работу по подготовке документов – 1000 ₽.</li>
                </ul>

                <p>Обращаясь к профессионалам, вы можете быть уверены в положительном результате.</p>
            </div>

            <div class="col-md-6">
                <img src="<?=get_template_directory_uri();?>/assets/images/liquidation-ip.jpg" alt="img">
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>