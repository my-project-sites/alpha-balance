<?php 
    # Template Name: Внесение изменений в ИП
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <h1 class="title title_first">Внесение изменений в ИП</h1>

                <p>
                    Зарегистрировав индивидуальную предпринимательскую деятельность, каждый гражданин РФ должен помнить о том, что любые изменения личных данных, места регистрации или фактического проживания, способов осуществления деятельности требуют обязательного извещения об этом соответствующие органы государственного контроля и регистрации. Это необходимо для своевременного внесения изменений в ЕГРИП.
                </p>

                <p>
                    Процедура извещения о произошедших изменениях предусматривает предоставление соответствующего правильно заполненного заявления (форма Р24001) в Инспекцию в течение трех дней. В случае отклонения от требований к заполнению указанной формы, инспекция может не принять заявление, в результате чего ИП попадет под действия штрафных санкций.
                </p>

            </div>

            <div class="col-md-6">
                <img src="<?=get_template_directory_uri();?>/assets/images/edit-ip.jpg" alt="img">
            </div>

            <div class="col-md-6">
                <ul class="markedList">
                    <li class="markedList__item">Паспортные данные индивидуального предпринимателя.</li>
                    <li class="markedList__item">
                        Место регистрации ИП (сообщение об изменении места регистрации необходимо направлять в «старую» службу).
                    </li>
                    <li class="markedList__item">Изменение правовых связей человека с государством.</li>
                    <li class="markedList__item">Удостоверение личности.</li>
                    <li class="markedList__item">Удостоверение личности иностранца.</li>
                    <li class="markedList__item">
                        Документ, предоставляющий право иностранцу вести предпринимательскую деятельность на территории РФ.
                    </li>
                    <li class="markedList__item">Основание для дееспособности несовершеннолетних (для лиц, не достигших восемнадцатилетия).</li>
                    <li class="markedList__item">Расширение предпринимательской деятельности.</li>
                    <li class="markedList__item">Исключение некоторых видов экономической деятельности.</li>
                </ul> 
            </div>

            <div class="col-md-12">
                <p class="font-weight-bold mt-3">
                    Представители налоговой службы самостоятельно заполняют расписку о получении документов, предъявляемых ИП для внесения изменений.
                </p>

                <p>Перечень документов, обязательных для внесения изменений в ЕГРИП:</p>

                <ul class="numberedList">
                    <li class="numberedList__item">
                        Удостоверение личности либо его копия (нотариально заверенная в случае подачи третьим лицом).
                    </li>
                    <li class="numberedList__item">Идентификационный код плательщика налогов.</li>
                    <li class="numberedList__item">
                        Копия документов, подтверждающих внесение предыдущих изменений в ЕГРИП (если таковые были).
                    </li>
                    <li class="numberedList__item">Контактный телефон.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>