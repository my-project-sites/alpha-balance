<?php 
    # Template Name: Контакты
	get_header(); 
	wp_reset_postdata();
?>

<script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

<div class="content">
    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <h1 class="title title_first">Контакты Альфа—Баланс</h1>    
            </div>

            <div class="col-md-6">
                <p>
                    Специалисты компании Альфа Баланс предоставляют в Калининграде компетентные консультации по вопросам ведения бухгалтерии на предприятии, сдачи отчетности и осуществления множества других операций. Предоставление компетентных консультаций – одно из важных направлений деятельности нашей компании. Своим клиентам мы оказываем помощь на бесплатной основе. При возникновении необходимости, вы можете отправить на наш e-mail адрес сообщение с вопросом – в ближайшем времени наши специалисты дадут на него исчерпывающий ответ.
                </p>

                <p><b>Адрес:</b> г. Калининград, Бизнес центр Акрополь офис 519 (5 этаж)</p>
                <p><b>Телефон:</b> 8(800)–222–73–10</p>

                <p class="mb-0"><b>Время работы:</b></p>

                <p>
                    Понедельник-пятница: c 09:00 до 18:00 часов<br>
                    Суббота-воскресенье: выходные
                </p>
            </div>

            <div class="col-md-6">
                <?=do_shortcode('[wpforms id="100"]');?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="gis-map"> 
                    <div id="map" style="width:100%; height:100%"></div>
                </div>
                
                <script>        
                    var map;
                    DG.then(function () {
                        map = DG.map('map', {
                            center: [54.721281, 20.506491],
                            zoom: 14,
                            scrollWheelZoom: false
                        });

                        DG.marker([54.721281, 20.506491]).addTo(map).bindPopup('ООО «АЛЬФА—БАЛАНС»');
                    });  
                </script>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>