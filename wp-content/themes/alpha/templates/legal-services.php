<?php 
    # Template Name: Юридические услуги
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <h1 class="title title_first">Представительство в суде</h1>

                <p>
                    Компания Альфа Баланс предлагает владельцам фирм и ИПэшникам качественное бухгалтерское обслуживание в Калининграде. Однако это не единственное направление деятельности нашей компании. Чтобы выжить в сложных современных экономических условиях, предпринимателям приходится учиться подстраиваться под самые различные ситуации. Мы помогаем в этом своим клиентам, оказывая широкий спектр юридических услуг:
                </p>

                <ul class="markedList">
                    <li class="markedList__item">
                        <a href="/likvidacziya-ooo/">Ликвидация ООО</a>
                    </li>
                    
                    <li class="markedList__item">
                        <a href="/vnesenie-izmenenij-v-ip/">Внесение изменений в ИП</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/vnesenie-izmenenij-v-ooo/">Внесение изменений в ООО</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/zakaz-vypisok/">Заказ выписок</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/izmenenie-sostava-uchastnikov-ooo/">Изменение состава участников ООО</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/likvidacziya-ip/">Ликвидация ИП</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/poluchenie-kopii-ustava/">Получение копии устава</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/prodazha-gotovyh-firm/">Продажа готовых фирм</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/predstavitelstvo-v-sude/">Представительство в суде</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/yuruslugi/registracziya-ip/">Регистрация ИП</a>
                    </li>

                    <li class="markedList__item">
                        <a href="/registracziya-ooo/">Регистрация ООО</a>
                    </li>
                </ul> 
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>