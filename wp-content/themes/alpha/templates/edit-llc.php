<?php 
    # Template Name: Внесение изменений в ООО
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
    <div class="container-xl">
        <div class="row">
            <div class="col-12">
                <h1 class="title title_first">Внесение изменений в ООО</h1>

                <p>
                    Любые изменения, связанные с адресом, родом деятельности или документацией участников ООО, требуют внесения соответствующих корректив в уставе предприятия. Многие начинающие бизнесмены считают, что это несложная операция. Но, вплотную подойдя к ее выполнению, понимают, что без помощи специалистов справиться не смогут. В компании Альфа Баланс работают настоящие профессионалы, которые помогут внести изменения в учредительные документы ООО или в состав участников ООО. Наши юристы помогут Вам грамотно оформить все коррективы, без проблем проведут несколько изменений, проконсультируют по поводу снижения финансовых и временных затрат на регистрацию изменений.
                </p>

                <h2 class="title title_first">В ООО могут вноситься изменения нескольких типов:</h2>

                <p class="font-weight-bold">Изменение учредительных документов:</p>

                <ul class="markedList">
                    <li class="markedList__item">Замена названия.</li>
                    <li class="markedList__item">Изменение адреса.</li>
                    <li class="markedList__item">Смена деятельности ООО.</li>
                    <li class="markedList__item">Изменение изначально инвестированной участниками общества суммы денежных средств.</li>
                    <li class="markedList__item">Учреждение нового представительства.</li>
                    <li class="markedList__item">Любые изменения, связанные с деятельностью директора общества.</li>
                    <li class="markedList__item">Уставные преобразования ООО.</li>
                </ul> 

                <p class="font-weight-bold">Изменения в госреестре (ЕГРЮЛ):</p>

                <ul class="markedList">
                    <li class="markedList__item">Замена названия.</li>
                    <li class="markedList__item">Личные данные директора, учредителя.</li>
                    <li class="markedList__item">Место проживания, паспортные данные директора, учредителя.</li>
                    <li class="markedList__item">Сфера деятельности общества.</li>
                    <li class="markedList__item">Операции по купле или продажи части бизнеса.</li>
                    <li class="markedList__item">Любые изменения, связанные с деятельностью директора общества.</li>
                    <li class="markedList__item">ООО лишилось кого-либо из участников.</li>
                </ul> 

                <p>Состав предоставленных клиенту услуг непосредственно отражается на ценовом диапазоне регистрации.</p>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>