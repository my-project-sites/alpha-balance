$(document).ready(function() {
    // Slinky menu
    if(document.documentElement.clientWidth > 992) {
        $('#navbarSupportedContent').removeClass('js-menu');
        $('nav').removeClass('bg-light');
    } 

    $('.js-menu').slinky({
        title: true
    });

    // Placeholder
    $('#wpforms-101-field_0').attr("placeholder", "Имя");
    $('#wpforms-101-field_5').attr("placeholder", "Email");

    // Ссылки
    $('#menu-item-21 > a, #menu-item-30 > a').on('click', function() {
        return false;
    })

    // Слайдер
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000
    });

});