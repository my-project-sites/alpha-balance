<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <?php wp_head(); ?>
</head>
<body>

<header>
	<div class="container-xl">
		<div class="row">
			<div class="col-md-4">
				<div class="logobox">
					<a href="/">
						<img src="<?=get_template_directory_uri();?>/assets/images/logo.png" alt="logo">
					</a>
					<p class="logobox__text">Ваша бухгалтерия — наша работа! Начните её со звонка нам!</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box">
					<img src="<?=get_template_directory_uri();?>/assets/images/location.png" alt="img">
					<p class="box__text">Калининград, <br>Бизнес центр Акрополь, <br>офис 574 (7 этаж)</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box">
					<img src="<?=get_template_directory_uri();?>/assets/images/phone.png" alt="img">
					<p class="box__text box__text_num">8 800 222 77 10 <br><span class="box__text_dlc">(звонок БЕСПЛАТНЫЙ)</span></p>
				</div>
			</div>
				
		</div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<span></span>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse js-menu" id="navbarSupportedContent">
				<?php
					wp_nav_menu(array(
						'theme_location'  => 'navbar',
						'container'       => false,
						'menu_class'      => 'menu',
						'fallback_cb'     => '__return_false',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					));
				?>
			</div>
		</div>
	</nav>
</header>