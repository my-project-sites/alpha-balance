<?php 
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="title title_first"><?=the_title();?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>