<?php

ini_set("session.use_trans_sid", 0);
ini_set("session.use_cookies", 0);

# Отладочная функция
function out($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

// Добавим класс элементам меню
function atg_menu_classes($classes, $item, $args) {
	if($args->theme_location == 'navbar') {
		$classes[] = 'menu__item';
	}
	return $classes;
}
add_filter('nav_menu_css_class','atg_menu_classes',1,3);

// Добавим класс ссылкам меню
function add_specific_menu_location_atts($atts, $item, $args) {
    if($args->theme_location == 'navbar') {
      $atts['class'] = 'menu__link';
    }
    return $atts;
}
add_filter('nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3);

// Стили для sub-menu
function change_wp_nav_menu($classes, $args, $depth) {
	$classes[] = 'my-css-1';
	$classes[] = 'my-css-2';

	return $classes;
}
add_filter('nav_menu_submenu_css_class', 'change_wp_nav_menu', 10, 3);

# Шорткод для home_url
add_shortcode('main_url', 'url_func');
function url_func() {
    return get_home_url();
}

# Шорткод для email admin
add_shortcode('userEmail', 'email_func');
function email_func() {
    return 'guyfoxes@protonmail.com';
}

# Шорткод для site_name
add_shortcode('site_name', 'site_name');
function site_name() {
    $name = get_home_url();
    $number = stripos($name, '//');
    return 'www.' . substr($name, $number + 2);
}

# Поддержка картинок записей
add_theme_support('post-thumbnails');

# Подключаем стили в Header
add_action('wp_enqueue_scripts', 'header_styles');
function header_styles() {
	wp_enqueue_style('reset', get_template_directory_uri() . '/assets/css/reset.css');
	wp_enqueue_style('OpenSans', get_template_directory_uri() . '/assets/font/OpenSans.css');
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('slick', get_template_directory_uri() . '/assets/css/slick.css');
	wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css');
	wp_enqueue_style('slinky', get_template_directory_uri() . '/assets/css/slinky.min.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
}

# Подключаем скрипты в шапке
add_action('wp_enqueue_scripts', 'header_scripts');
function header_scripts() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.4.1.min.js');
	wp_enqueue_script('jquery');
}

# Подключаем скрипты в footer
add_action('wp_footer', 'footer_scripts');
function footer_scripts() {
	wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js');
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	wp_enqueue_script('slinky', get_template_directory_uri() . '/assets/js/slinky.min.js');
	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js');
}

# Отключаем Гутенберг
if('disable_gutenberg') {
	add_filter('use_block_editor_for_post_type', '__return_false', 100);
	remove_action('wp_enqueue_scripts', 'wp_common_block_scripts_and_styles');
	add_action('admin_init', function() {
		remove_action('admin_notices', ['WP_Privacy_Policy_Content', 'notice']);
		add_action('edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
	});
}

# Удалить атрибут type у styles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}

# Удалить атрибут type у scripts 
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

# Асинхронная загрузка для скриптов, подключенных через wp_enqueue_script
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function add_async_attribute($tag, $handle) {
	if(!is_admin()) {
	    if ('jquery-core' == $handle) {
	        return $tag;
	    }
	    return str_replace(' src', ' defer src', $tag);
	}
	else {
		return $tag;
	}
}

# Удалим метки
add_action('init', 'unregister_taxonomy_post_tag'); 
function unregister_taxonomy_post_tag() {
	register_taxonomy('post_tag', array());
}

// Длина выводимового ткста
add_filter('excerpt_length', function() {
	return 20;
});

# Класс для использования menu Bootstrap
class bootstrap_4_walker_nav_menu extends Walker_Nav_menu {
    
    function start_lvl(&$output, $depth = 0, $args = null) { 
        $indent = str_repeat("\t",$depth); 
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
    }
  
  	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        
    $indent = ($depth) ? str_repeat("\t",$depth) : '';
    
    $li_attributes = '';
        $class_names = $value = '';
    
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        
        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
        $classes[] = 'nav-item';
        $classes[] = 'nav-item-' . $item->ID;
        if( $depth && $args->walker->has_children ){
            $classes[] = 'dropdown-menu';
        }
        
        $class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args ) );
        $class_names = ' class="' . esc_attr($class_names) . '"';
        
        $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
        
        $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';
        
        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';
        
        $attributes .= ( $args->walker->has_children ) ? ' class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="nav-link"';
        
        $item_output = $args->before;
        $item_output .= ( $depth > 0 ) ? '<a class="dropdown-item"' . $attributes . '>' : '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        
        $output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

# Register Navbar
register_nav_menu('navbar', __('Navbar', 'Основное меню'));

# Подключение стилей в админке
add_action('admin_head', 'wph_inline_css_admin');
function wph_inline_css_admin() { echo 
	'<style>
		.plugin-title span a[href="http://www.never5.com/?utm_source=plugin&utm_medium=link&utm_campaign=what-the-file"],
		.wramvp_img_email_image,
		.upgrade_menu_link, 
		.proupgrade, .notice-error, .column-RV,
		.column-MV, .woocommerce-help-tip,
		.updated, #dashboard_php_nag,
		.mailpoet-dismissible-notice,
		.skiptranslate,
		.mailpoet_feature_announcement,
		.elementor-plugins-gopro,
		.aioseop-notice-woocommerce_detected,
		.aioseop_nopad_all,
		.aioseop_help_text_link, 
		.aioseop-metabox-pro-cta,
		aioseop-sitemap-prio-upsell,
		#aiosp_license_key_wrapper,
		#aioseop-notice-bar,
		.aioseop-sitemap-prio-upsell,
		.aioseop-notice-review_plugin_cta,
		.help_tip,
		.wbcr-upm-plugin-status,
		#wp-admin-bar-view-site,
		#wp-admin-bar-aioseop-pro-upgrade,
		.aioseo-upgrade-bar,
		#wp-admin-bar-aioseo-pro-upgrade,
		.aioseo-submenu-highlight,
		.wbcr-clearfy-factory-notice,
		.aioseo-score-button,
		.rank-math-column-display.no-score,
		.aioseo-review-plugin-cta
		{
			display: none !important;
		}
		#thumb {
			color: transparent;
		}
		.aioseo-google-search-preview {
			overflow: hidden;
		}
	</style>';
}

?>