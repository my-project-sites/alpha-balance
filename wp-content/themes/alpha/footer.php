<footer id="footer">
	<div class="container-fluid blu">
		<div class="row">
			<div class="col-12">
				<div class="developer">
					<a target="_blank" href="https://www.facebook.com/bloodborne.gothic">
						<img class="developer__logo" src="<?=get_template_directory_uri();?>/assets/images/my-logo.webp" alt="logo">
					</a>
				</div>
				<p class="mt-4 mb-0 text-center">2020 © Компания Альфа Баланс — бухгалтерские и юридические услуги в Калининграде.</p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>