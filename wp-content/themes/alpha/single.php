<?php 
	get_header();
	wp_reset_postdata(); 
?>

<div class="wrapper">
    <div class="container-xl">
        <div class="row">
            <div class="col-lg-9 content">
                <h1 class="title title_first mb-5"><?php the_title(); ?></h1>
                <?php the_content(); ?>
                <a class="slider__link" href="<?=do_shortcode('[linkReferal]');?>">Регистрация</a>
            </div>

            <div class="col-lg-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>