<?php

# Имя базы данных для WordPress
define('DB_NAME', 'alpha');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'IU`WjI zeiS6XSq&*Y9&,XK@ns;7+l416bqCGR!0J[5:]=9%R=8?#m[z@lepNNus');
define('SECURE_AUTH_KEY',  'FQ{WN#X)1$F(F#jx-b,2)zRdL`nv)OqjIqh)(!lZK!=w`U9aNFfmW+6s&CE~q6[_');
define('LOGGED_IN_KEY',    '-1D,:&W4?-m{unwz]n&Vj[zJX,[rt82[JW~2wKLd,OfB8AVMls<:=/oc[s|!3lo;');
define('NONCE_KEY',        '!_g-Rdfh6 ^M$o2Wwfb_ ;OgQ/EoaL<|N:V&4vio|]$u-xk)^O@e:/E*GD@GfJ.^');
define('AUTH_SALT',        'hZYT?46aIrR>}g&,XS7e7VtFsq^A])N97v}hGl)kkG|dg1+(`,027:S=_&:p_Y]{');
define('SECURE_AUTH_SALT', 'Q4yI)+jcxov^{nLWr}&uP=1n&6db]C`l?sfi<=.AZA**s[k=>osc>/ERfEQtDaB5');
define('LOGGED_IN_SALT',   '-J8%]F{NM~=4waA4$>N2&Zt`ig`.b#R1j2Du?j@[4A+*?3HT%_FHMC=I%45K18J+');
define('NONCE_SALT',       '_WLe2E-|&<g:J@[-*cNPGoN~z*9Z1?%[6g?<pb-5uwH!@aGYg5fJG `K.l;CFe4y');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */

$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */

define('WP_DEBUG', false);

/** Абсолютный путь к директории WordPress. */

if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

# Инициализирует переменные WordPress и подключает файлы.
require_once ABSPATH . 'wp-settings.php';